import os
from datetime import datetime
from loguru import logger
import pandas as pd
import spacy
import spacy_transformers
from spacy.tokens import DocBin
import en_core_web_trf
import tensorflow as tf
import tensorflow_text
import tensorflow_hub as hub
from official.nlp import optimization  # to create AdamW optimizer
from utils.utilities import read_and_split_training_data_into_test_train

SCRIPT_PATH = os.path.dirname(__file__)
BERT_MODEL_PATH = os.path.join(SCRIPT_PATH, "trained_models", "yelp_review_bert")
SPACY_MODEL_PATH = os.path.join(SCRIPT_PATH, "trained_models", "model-best")


def infer_sentiments(list_of_examples:list, model:str) -> list:
    """Function generates a list of predictions given a list of sentiments"""
    if model == "bert":
        TF_MODEL = tf.keras.models.load_model(
            BERT_MODEL_PATH, compile=False
        )
        logger.info("bert model was loaded successfully")
        sentiment_results = tf.sigmoid(TF_MODEL(tf.constant(list_of_examples)))
        return sentiment_results
    if model == "spacy":
        SPACY_MODEL = spacy.load(SPACY_MODEL_PATH)
        logger.info("spacy model was loaded successfully")
        sentiments = [SPACY_MODEL(text) for text in list_of_examples]
        results = [list(result.cats.values()) for result in sentiments]
        return results

def generate_spacy_training_document_list(
    training_df:pd.DataFrame, 
    model:spacy.language.Language
) -> list:
    """Function generates and saves spacy documents from a pandas dataframe"""
    train_docs = []
    counter = 0
    for doc, label in model.pipe(
        training_df.text_label_pair.to_list(), as_tuples = True
    ):
        counter += 1
        if (label==1):
            doc.cats['positive'] = 1
            doc.cats['negative'] = 0
            doc.cats['neutral']  = 0
        elif (label==0):
            doc.cats['positive'] = 0
            doc.cats['negative'] = 1
            doc.cats['neutral']  = 0
        else:
            doc.cats['positive'] = 0
            doc.cats['negative'] = 0
            doc.cats['neutral']  = 1
        train_docs.append(doc)
        if counter%100==0 or counter==1:
            logger.info(f"completed first {counter} rows")
    return train_docs


def generate_spacy_sentiments_training_data():
    """Function to manage the generation of spacy training data"""
    nlp_trf = en_core_web_trf.load()
    logger.info("transformer model was loaded..")
    training_data_folder = os.path.join(
        SCRIPT_PATH, 
        "training_data"
    )
    training_data_path = os.path.join(
        training_data_folder, 
        "training_data_sm.parquet.gzip"
    )
    train_df, val_df = read_and_split_training_data_into_test_train(
        training_data_path,
        test_data=False
    )
    train_df['text_label_pair'] = train_df.apply(
        lambda row: (row['text'],row['sentiment']), 
        axis=1
    )
    logger.info("Initiating coversion of training data into spacy documents..")
    train_docs = generate_spacy_training_document_list(train_df, nlp_trf)
    train_binary_document = DocBin(docs = train_docs)
    spacy_train_output_path = os.path.join(
        training_data_folder, 
        "train.spacy"
    )
    train_binary_document.to_disk(spacy_train_output_path)
    logger.info(f"Spacy training documents has been written to {spacy_train_output_path}")
    val_df['text_label_pair'] = val_df.apply(
        lambda row: (row['text'],row['sentiment']), 
        axis=1
    )
    logger.info("Initiating coversion of validation data into spacy documents...")
    val_docs = generate_spacy_training_document_list(val_df, nlp_trf)
    val_binary_document = DocBin(docs = val_docs)
    spacy_val_output_path = os.path.join(
        training_data_folder, 
        "test.spacy"
    )
    val_binary_document.to_disk(spacy_val_output_path)
    logger.info(f"Spacy validation documents has been written to {spacy_val_output_path}")


def build_tensorflow_classifier_model(
    tfhub_handle_for_encoder_model:str, 
    tfhub_handle_for_preprocessor:str
) -> tf.keras.Model:
    """Function to create the bert calssifier model"""
    text_input = tf.keras.layers.Input(shape=(), dtype=tf.string, name='text')
    preprocessing_layer = hub.KerasLayer(tfhub_handle_for_preprocessor, name='preprocessing')
    encoder_inputs = preprocessing_layer(text_input)
    encoder = hub.KerasLayer(tfhub_handle_for_encoder_model, trainable=True, name='BERT_encoder')
    outputs = encoder(encoder_inputs)
    net = outputs['pooled_output']
    net = tf.keras.layers.Dropout(0.1)(net)
    net = tf.keras.layers.Dense(1, activation=None, name='classifier')(net)
    return tf.keras.Model(text_input, net)

def train_tensorflow_model(epochs:int=3):
    """Function to manage the training of tensorflow bert model"""
    current_datetime = datetime.now().strftime("%Y_%m_%d__%H_%M_%S")
    training_data_folder = os.path.join(
        SCRIPT_PATH, 
        "training_data"
    )
    trained_models_folder = os.path.join(
        SCRIPT_PATH, 
        "trained_models"
    )
    training_data_path = os.path.join(
        training_data_folder, 
        "training_data_sm.parquet.gzip"
    )
    train_df, val_df, test_df = read_and_split_training_data_into_test_train(
        training_data_path,
        test_data=True
    )
    train_ds = tf.data.Dataset.from_tensor_slices((train_df.text, train_df.sentiment))
    val_ds = tf.data.Dataset.from_tensor_slices((val_df.text, val_df.sentiment))
    test_ds = tf.data.Dataset.from_tensor_slices((test_df.text, test_df.sentiment))

    train_ds  = train_ds.batch(32)
    val_ds  = val_ds.batch(32)
    test_ds  = test_ds.batch(32)

    tfhub_handle_for_encoder_model = 'https://tfhub.dev/tensorflow/albert_en_base/2'
    tfhub_handle_for_preprocessor = 'https://tfhub.dev/tensorflow/albert_en_preprocess/3'

    logger.info(f'BERT model selected           : {tfhub_handle_for_encoder_model}')
    logger.info(f'Preprocess model auto-selected: {tfhub_handle_for_preprocessor}')

    classifier_model = build_tensorflow_classifier_model(
        tfhub_handle_for_encoder_model,
        tfhub_handle_for_preprocessor
    )
    loss = tf.keras.losses.BinaryCrossentropy(from_logits=True)
    metrics = tf.metrics.BinaryAccuracy()

    AUTOTUNE = tf.data.AUTOTUNE
    train_ds = train_ds.cache().prefetch(buffer_size=AUTOTUNE)
    val_ds = val_ds.cache().prefetch(buffer_size=AUTOTUNE)
    test_ds = test_ds.cache().prefetch(buffer_size=AUTOTUNE)

    epochs = epochs
    steps_per_epoch = tf.data.experimental.cardinality(train_ds).numpy()
    num_train_steps = steps_per_epoch * epochs
    num_warmup_steps = int(0.1*num_train_steps)
    init_lr = 3e-5

    optimizer = optimization.create_optimizer(
        init_lr=init_lr,
        num_train_steps=num_train_steps,
        num_warmup_steps=num_warmup_steps,
        optimizer_type='adamw'
    )
    early_stopping = tf.keras.callbacks.EarlyStopping(
        monitor='val_binary_accuracy', 
        verbose=1,
        patience=1,
        mode='max',
        restore_best_weights=True
    )
    checkpoint_path = os.path.join(
        trained_models_folder,
        f'{current_datetime}_training',
        'cp.ckpt'
    )
    cp_callback = tf.keras.callbacks.ModelCheckpoint(
        filepath=checkpoint_path, 
        save_weights_only=True, 
        verbose=1
    )
    classifier_model.compile(
        optimizer=optimizer,
        loss=loss,
        metrics=metrics
    )
    logger.info(f'Training model with {tfhub_handle_for_encoder_model}')
    _history = classifier_model.fit(
        x=train_ds,
        validation_data=val_ds,
        callbacks = [early_stopping, cp_callback],
        epochs=epochs
    )
    loss, accuracy = classifier_model.evaluate(test_ds)

    logger.info(f'Model Loss: {loss}')
    logger.info(f'Model Accuracy: {accuracy}')
    model_save_path = os.path.join(
        trained_models_folder,
        f'{current_datetime}_bert_model'
    )
    model_weights_path = os.path.join(
        trained_models_folder,
        f'{current_datetime}_final_checkpoint'
    )

    classifier_model.save(model_save_path, include_optimizer=False)
    classifier_model.save_weights(model_weights_path)
    logger.info(f'Model has been saved to: {model_save_path}')
    logger.info(f'Final model weights saved to: {model_weights_path}')
    logger.info(f'Training process has been completed')
