""" Main entry point script placeholder for managing model inference and training"""
import click
from loguru import logger
from models.sentiments_analysis import (
    infer_sentiments,
    generate_spacy_sentiments_training_data,
    train_tensorflow_model
)
from utils.utilities import print_sentiment_results


@click.command()
@click.option(
    "--training", default=False, help="Switch between training and inference mode"
)
@click.option("--text", default=None, help="Text to perform inference on")
@click.option(
    "--task",
    default="sentiments",
    type=click.Choice(["ner", "sentiments"]),
    help="Toggle between sentiment analysis and NER tasks",
)
@click.option(
    "--modeltype",
    default="spacy",
    type=click.Choice(["bert", "spacy"]),
    help="Specify model to use for sentiment analysis: bert or spacy",
)
def entrypoint(training, text, task, modeltype):
    """Entrypoint function for model sentiments prediction and training."""
    logger.info(f"Training mode: {training}")
    logger.info(f"Task selected: {task}")
    logger.info(f"Model type selected: {modeltype}")
    if training and modeltype=='bert' and task=='sentiments':
        logger.info("Model will be run in training mode")
        train_tensorflow_model()
    elif training and modeltype=='spacy' and task=='sentiments':
        logger.info("Model will be run in training mode")
        generate_spacy_sentiments_training_data()
    elif not training and task=='sentiments':
        examples = [
            "this is such an amazing experience!",
            "The food was great!",
            "The taste of the drinks was meh.",
            "The movie was okish.",
            "The place was really difficult to leave, you would want to stay quite long there because of how well they treat you",
        ]
        logger.info(f"Inference process initiated")
        logger.info("Results from the model:")
        if text:
            positive_sentiments = infer_sentiments([text], modeltype)
            print_sentiment_results([text], positive_sentiments)
        else:
            positive_sentiments = infer_sentiments(examples, modeltype)
            print_sentiment_results(examples, positive_sentiments)

if __name__ == "__main__":
    entrypoint()
