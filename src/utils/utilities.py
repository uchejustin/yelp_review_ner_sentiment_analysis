import pandas as pd
from loguru import logger
from sklearn.model_selection import train_test_split

def print_sentiment_results(inputs, results):
    result_for_printing = [
        f"input: {inputs[i]:<30} : score: {results[i][0]:.6f}"
        for i in range(len(inputs))
    ]
    logger.info("The results of the inference are as follows:")
    for result in result_for_printing:
        logger.info(result)

def read_and_split_training_data_into_test_train(
    data_path:str, 
    test_data:bool=False
) -> tuple:
    """Function splits training data into test and train splits"""
    training_data = pd.read_parquet(data_path)
    if test_data:
        _train_df, test_df = train_test_split(
            training_data, 
            test_size=0.2, 
            random_state=42, 
            shuffle=True, 
            stratify=training_data.sentiment
        )
        train_df, val_df = train_test_split(
            _train_df, 
            test_size=0.2, 
            random_state=42, 
            shuffle=True, 
            stratify=_train_df.sentiment
        )
        logger.info("Training data has been split into the following sizes: ")
        logger.info(f"Training dataframe shape: {train_df.shape}")
        logger.info(f"Validation dataframe shape: {val_df.shape}")
        logger.info(f"Test dataframe shape: {test_df.shape}")
        return train_df, val_df, test_df
    else:
        train_df, val_df = train_test_split(
            training_data, 
            test_size=0.3, 
            random_state=42, 
            shuffle=True, 
            stratify=training_data.sentiment
        )
        logger.info("Training data has been split into the following sizes: ")
        logger.info(f"Training dataframe shape: {train_df.shape}")
        logger.info(f"Validation dataframe shape: {val_df.shape}")
        return train_df, val_df
