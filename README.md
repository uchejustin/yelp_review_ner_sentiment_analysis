# A yelp review named-entity recognition and sentiment analysis

## Description
An implementation of a named entity recognition and sentiment analysis using Yelp review open dataset in a learning collaboration project.

TBD..

## Installation
```
python -m pip install --upgrade pip
python -m pip install -r requirements.txt
python -m spacy download en_core_web_trf
```

## Executing script for training
    To view help text for cmd options
    - python main.py --help

    Training is carried out using a parquet zipped file provided in models/taining_data folder
    Training data is expected to have a text and a sentiment column
    Sentiment column contains values 0 or 1 representing negative or positive sentiments
    Text columns is expected to have english text representing the sentiment comments
    
    To perform training on model using spacy, first generate spacy training documents
    - python main.py --training True --task sentiments --modeltype spacy
    Then train model with your provided DATA_FOLDER variable:
    - python -m spacy train $DATA_FOLDER/config.cfg --paths.train $DATA_FOLDER/train.spacy --paths.dev $DATA_FOLDER/test.spacy --output $DATA_FOLDER/spacy_output --verbose

    To perform training on model using bert
    - python main.py --training True --task sentiments --modeltype bert


## Executing script for inference
    To perform inference on text input
    - python main.py --training False --text "some excellent text to evaluate" --task ner --modeltype spacy


## To contribute

```
cd yelp_review_ner_sentiment_analysis  #downstream repository stored locally
git checkout -b feature/some-task-name  #create feature branch
git add some-new-file
git commit -m "changes made to new file"
git push --set-upstream origin feature/some-task-name  #push new branch and create a Pull Request
git checkout main  #after PR is accepted
git pull
```

## Authors and acknowledgment
Special mention to:
- Uche Nwachukwu
- Tosin Oyewale

## License
This work is permissive with no restrictions on usage or modifications.
